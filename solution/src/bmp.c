#include "../include/bmp.h"
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BMP_HEADER_SIZE 40
#define BITS_PER_PIXEL 24
#define BYTES_PER_PIXEL (BITS_PER_PIXEL / 8)
#define BMP_FILE_TYPE 0x4D42
#define BMP_PADDING 4

typedef struct bmp_header BMPHeader;

int calculate_padding(int width) {
    return (BMP_PADDING - (width * BYTES_PER_PIXEL) % BMP_PADDING) % BMP_PADDING;
}

enum read_status from_bmp(const char* filepath, struct image* img) {
    FILE* file = fopen(filepath, "rb");
    if (!file) {
        return READ_ERROR;
    }

    BMPHeader header;
    fread(&header, sizeof(BMPHeader), 1, file);

    if (header.bfType != BMP_FILE_TYPE) {
        fclose(file);
        return READ_INVALID_SIGNATURE;
    }

    img->width = (int) header.biWidth;
    img->height = (int) header.biHeight;
    img->data = (struct pixel*) malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) {
        fclose(file);
        return READ_INVALID_BITS;
    }

    int imgWidth = (int) img->width;
    int imgHeight = (int) img->height;

    for (int y = imgHeight - 1; y >= 0; y--) {
        for (int x = 0; x < imgWidth; x++) {
            struct pixel p;
            fread(&p, sizeof(struct pixel), 1, file);
            img->data[y * imgWidth + x] = p;
        }
        for (int padding = 0; padding < calculate_padding(imgWidth); padding++) {
            fgetc(file);
        }
    }

    fclose(file);
    return READ_OK;
}

enum write_status to_bmp(const char* filepath, struct image const* img) {
    FILE* file = fopen(filepath, "wb");
    if (!file) {
        return WRITE_ERROR;
    }

    int imgWidth = (int) img->width;
    int imgHeight = (int) img->height;

    BMPHeader header = {
        .bfType = BMP_FILE_TYPE,
        .bfileSize = BMP_HEADER_SIZE + (imgWidth * BYTES_PER_PIXEL + calculate_padding(imgWidth)) * imgHeight,
        .bfReserved = 0,
        .bOffBits = BMP_HEADER_SIZE,
        .biSize = BMP_HEADER_SIZE,
        .biWidth = imgWidth,
        .biHeight = imgHeight,
        .biPlanes = 1,
        .biBitCount = BITS_PER_PIXEL,
        .biCompression = 0,
        .biSizeImage = (imgWidth * BYTES_PER_PIXEL + calculate_padding(imgWidth)) * imgHeight,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };

    fwrite(&header, sizeof(BMPHeader), 1, file);

    for (int y = imgHeight - 1; y >= 0; y--) {
        for (int x = 0; x < imgWidth; x++) {
            struct pixel p = img->data[y * imgWidth + x];
            fputc(p.b, file);
            fputc(p.g, file);
            fputc(p.r, file);
        }
        for (int padding = 0; padding < calculate_padding(imgWidth); padding++) {
            fputc(0x00, file);
        }
    }

    fclose(file);
    return WRITE_OK;
}
