#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare(const void *a, const void *b) {
    return (*(int*)a - *(int*)b);
}

int main(int argc, char* argv[]) {

    if (argc != 4) {
        fprintf(stderr, "Invalid number of arguments\nUsage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    char* source_file = argv[1];
    char* transformed_file = argv[2];
    int angle = atoi(argv[3]);

    if (angle % 90 != 0 && angle < -270) {
        printf("Введите верный угол\n");
        return 1;
    }

    struct image source_image = { 0 };
    if (from_bmp(source_file, &source_image) != READ_OK) {
        fprintf(stderr, "При чтении исходного изображения произошла ошибка\n");
        return 1;
    }

    struct image transformed_image = rotate(&source_image, angle);

    if (to_bmp(transformed_file, &transformed_image) != WRITE_OK) {
        fprintf(stderr, "Ошибка при записи преобразованного изображения.\n");
        return 1;
    }

    free(source_image.data);
    free(transformed_image.data);

    return 0;
}
